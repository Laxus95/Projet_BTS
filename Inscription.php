<?php

include ('header.php');
include ('Connexion_BDD.php');
?>

<title>S'inscrire</title>

<?php

//Vérification de la véracité des informations

if ($_POST['mail'] == "" || $_POST['pass'] == "" || $_POST['confirmepass'] == "" || $_POST['pass']!=$_POST['confirmepass']|| $_POST['nom'] == ""|| $_POST['prenom'] == "" || $_POST['ligne'] == "") 
	{
    	echo "Erreur de Saisie, veuillez renseigner correctement chaque champs...";
    }

else
    {


    	if (filter_var($_POST['mail'], FILTER_VALIDATE_EMAIL))
    	{


//Appel de la fonction de connexion à la bdd
 			$bdd=BDD();



//Affectation des valeurs des champs à des variables
			$email = $_POST['mail'];
			$mdp = $_POST['pass'] ;
			$nom=$_POST['nom'] ;
			$prenom=$_POST['prenom'] ;
			$ligne=$_POST['ligne'] ;

// Préparation de la requete
			$statement = $bdd->prepare("INSERT INTO utilisateurs(email,mdp,nom,prenom,ligne) VALUES (:email,:mdp,:nom,:prenom,:ligne)");

			$statement->bindParam(':email',$email,PDO::PARAM_STR);
			$statement->bindParam(':mdp',$mdp, PDO::PARAM_STR);
			$statement->bindParam(':nom',$nom, PDO::PARAM_STR);
			$statement->bindParam(':prenom',$prenom, PDO::PARAM_STR);
			$statement->bindParam(':ligne',$ligne, PDO::PARAM_STR);
//Execution de la requete
			$statement->execute();
//Message de réussite
			echo 'Insertion réussie </br>';
			echo "Bienvenue ".$email.", pour configurer votre profil veuillez renseigner vos informations dans la rubrique 'Mon Compte'. ";
    	}
    	else echo "Adresse email non valide";
	}

include ('footer.php');
?>