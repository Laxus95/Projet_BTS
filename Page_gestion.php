<?php
include ('header.php');
include ('Connexion_BDD.php');
?>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<title> Gérer formateurs </title>

<?php
//récupération des variables de session
if(($_SESSION['profil']=='Administrateur')||($_SESSION['profil']=='Gestionnaire'))
{


  $email2 = $_SESSION['email_session'];
  $id = $_SESSION['id_session'];
  $mdp_user= $_SESSION['mdp_session'];
  $ligne_form=$_SESSION['ligne_user'];
  }

  $message="";

  if (empty($email2)|| empty($id)) // Intrusion
  {
    $message='Vous ne possédez pas les autorisations nécessaires pour accéder aux informations de ce compte';
    header('Location: Accueil.php');
  }

  else
  {
                  
//Appel de la fonction de connexion à la bdd
    $bdd=BDD();
  
  ?>

<!-- Tableau présentation des données-->
<div class="container">
	<div class="row">
		<div class="paragraphe">
        
        <div class="col-md-12">
          <h4>Gestion membre de la ligne</h4>
          Pour filtrer la recherche, entrez un nom
          <form action="Page_gestion.php" method="POST"> 
            <input type="text" placeholder="Entrer un nom" name="filtre"/>
            <button type='submit' name='Filtrage' class='btn btn'> Rechercher </button>
        </form>
          <div class="table-responsive">
          
          <a class="police5" data-toggle='tooltip' title='Edit'><button class='btn btn-primary btn-xs' data-title='Edit' data-toggle='modal' data-target="#Ajout">+Ajouter</a></button>
        
          <div class="table-responsive">

                
              <table id="gestion" class="table table-bordred table-striped">
                   
                <thead>
                   
                   
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Nombre d'années</th>
                    <th>Email</th>
                    <th>Ce que j'aime le plus</th>
                    <th>Formation A</th>
                    <th>Formation B</th>
                    <th>Formation C</th>
                    <th>Ligne</th>
                     
                    <th>Modifier</th>                     
                    <th>Supprimer</th>
                </thead>
                <tbody>
    
<?php
    // Requete de récupération des données sur les formateurs
        if ($_SESSION['profil']=='Gestionnaire')
          {
             if (isset($_POST['Filtrage']))
            {
              $nomFiltre=$_POST['filtre'];
              $requete=$bdd->prepare("SELECT nom, prenom, description, Formation_A, Formation_B, Formation_C, annee, email, Ligne, id_Form FROM formateurs WHERE ligne=:ligne AND nom like '%".$nomFiltre."%'");
            }
            else
            {
              $requete=$bdd->prepare("SELECT nom, prenom, description, Formation_A, Formation_B, Formation_C, annee, email, Ligne, id_Form FROM formateurs WHERE ligne=:ligne");
            }
          

          $requete->bindParam(':ligne', $ligne_form, PDO::PARAM_STR);
          }
          if ($_SESSION['profil']=='Administrateur')
          {
             if (isset($_POST['Filtrage']))
            {
              $nomFiltre=$_POST['filtre'];
              
              $requete=$bdd->prepare("SELECT nom, prenom, description, Formation_A, Formation_B, Formation_C, annee, email, Ligne, id_Form FROM formateurs WHERE nom like '%".$nomFiltre."%' ORDER BY Ligne ");
            }
            else
            {
              $requete=$bdd->prepare("SELECT nom, prenom, description, Formation_A, Formation_B, Formation_C, annee, email, Ligne, id_Form FROM formateurs ORDER BY Ligne");
            }
          }
        
        
       
//Execution 
        $requete->execute();
// Boucle des données du tableau pour chaque formateur afin de tout afficher 
        while ($ligne=$requete->fetch())
        {
          $nom=$ligne['nom'];
          $prenom=$ligne['prenom'];
          $description=$ligne['description'];
          $form_A=$ligne['Formation_A'];
          $form_B=$ligne['Formation_B'];
          $form_C=$ligne['Formation_C'];
          $email=$ligne['email'];
          $annee=$ligne['annee'];
          $etablissement=$ligne['Ligne'];
          $id_form=$ligne['id_Form'];
          $DD="DD";
          $id_form2=$DD.$ligne['id_Form'];

// Tableau
          print  "<tr>
                    <td>".$nom."</td>
                    <td>".$prenom."</td>
                    <td>".$annee."</td>
                    <td>".$email."</td>
                    <td>".$description."</td>
                    <td>".$form_A."</td>
                    <td>".$form_B."</td>
                    <td>".$form_C."</td>
                    <td>".$etablissement."</td>
                    <td><p data-placement='top' data-toggle='tooltip' title='Edit'><button class='btn btn-primary btn-xs' data-title='Edit' data-toggle='modal' data-target='#".$id_form."'><span class='glyphicon glyphicon-pencil'></span></button></p></td>
                    <td><p data-placement='top' data-toggle='tooltip' title='Supprimer'><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' data-target='#".$id_form2."'><span class='glyphicon glyphicon-trash'></span></button></p></td>    
                  </tr>";
     



?>

<!-- Modification des données d'un formateur -->
<form action="ModifBDD_Gestion.php" method="POST">

<?php 

//Formulaire de modification des données suivant chaque personne du tableau: $id
print"<div class='modal fade' id=".$id_form." name='id' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>" ?>
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
          <h4 class="modal-title custom_align" id="Heading">Modifier les données</h4>
        </div>
      <div class="modal-body">
        <div class="form-group">
  <?php 

// Affectation des valeurs de base des champs du formulaire
print  " Nom <input class='form-control' type='text' value=".$nom." name='Nom'/>
        </div>
            <input type='hidden' name='id_formateur' value=".$id_form."/>
        <div class='form-group'> Prénom      
            <input class='form-control' type='text' value=".$prenom." name='Prenom'/>
        </div>";
        if($_SESSION['profil']=='Administrateur')
        {
                             
?>
           <div> Ligne</div>
              <div>
                <div>
                <input type='radio' name='ligne_form' value="D&R"/>
                  D&R 
                               
                <input  type='radio' name='ligne_form' value="C"/>C  

                <input  type='radio' name='ligne_form' value="EPT4"/>EPT4
                </div>                
                <input  type='radio' name='ligne_form' value="HBK"/>
                HBK                                         
                <input  type='radio' name='ligne_form' value="LAJ"/>LAJ
               
                <input  type='radio' name='ligne_form' value="N&U"/>
                N&U             
                <input  type='radio' name='ligne_form' value="UTN"/>UTN               
            </div> 
            
<?php
}
if($_SESSION['profil']=='Gestionnaire')
{
  print "<input type='hidden' name='ligne_form' value=".$etablissement."/>";
}


        print "
          <div class='form-group'> Nombre d'années       
            <input class='form-control' type='number' value=".$annee." name='Annee'/>
          </div>

        <div class='form-group'> Email
            <input class='form-control' type='email' name='Email' placeholder='Email' value=".$email." >
        </div>

        <div class='form-group'> Ce que j'aime le plus
            <textarea rows='2' class='form-control'  name='description'>".$description."</textarea>
        </div>

        <div class='form-group'> Formation A
            <textarea rows='2' class='form-control'  name='formA'>".$form_A."</textarea>
        </div>

        <div class='form-group'> Formation B
            <textarea rows='2' class='form-control' name='formB'>".$form_B."</textarea>
        </div>

        <div class='form-group'> Formation C
            <textarea rows='2' class='form-control' name='formC'>".$form_C."</textarea>
        </div>
  </div>
        <div class='modal-footer'>
            <button type='submit' name='Gestion_modif' class='btn btn-warning btn-lg' style='width: 100%;''><span class='glyphicon glyphicon-ok-sign'></span> Modifier </button>
        </div>
  </form>";

    
      ?>

     
              
            </div>
            
        </div>
    </div>
  </div>
</div>


 
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>
    

  <!-- Suppression d'un formateur -->
    <form action="ModifBDD_Gestion.php" method="POST">
    
    <?php print "<div class='modal fade' id=".$id_form2." tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>"; ?>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            <h4 class="modal-title custom_align" id="Heading">Suppression des données</h4>
          </div>
          <div class="modal-body">
            <?php print "<input type='hidden' name='id_formateur' value=".$id_form."/>"; ?>
              <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Etes vous sûr de vouloir supprimer cette personne?</div>
       
      
            <div class="modal-footer ">
              <button type="submit" name='Gestion_destruct' class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Oui</button>
              <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Non</button>

            </div>
      </form>
         
    

<?php
  }
  ?>
         </tbody>        
      </table>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>

    <!-- Formulaire ajout de membre -->

<form action="ModifBDD_Gestion.php" method="POST">
    
    <div class='modal fade' id=Ajout tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>


<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
          <h4 class="modal-title custom_align" id="Heading">Ajouter un membre</h4>
        </div>
      <div class="modal-body">
        <div class="form-group">
        <?php if ($_SESSION['profil']=='Gestionnaire')
        {
          print "<input type='hidden' name='ligne_form' value=".$_SESSION['ligne_user'].">";
        }  
        ?>
            Nom <input class='form-control' type='text' name='Nom'/>
        </div>

        <div class='form-group'>
            Prénom <input class='form-control' type='text'  name='Prenom'/>
        </div>
<?php
if($_SESSION['profil']=='Administrateur')
        {
                             
?>
           <div> Ligne</div>
              <div>
                <div>
                <input type='radio' name='ligne_form' value="D&R"/>
                  D&R 
                               
                <input  type='radio' name='ligne_form' value="C"/>C  

                <input  type='radio' name='ligne_form' value="EPT4"/>EPT4
                </div>                
                <input  type='radio' name='ligne_form' value="HBK"/>
                HBK                                         
                <input  type='radio' name='ligne_form' value="LAJ"/>LAJ
               
                <input  type='radio' name='ligne_form' value="N&U"/>
                N&U             
                <input  type='radio' name='ligne_form' value="UTN"/>UTN               
            </div> 
            
<?php
} ?>
        <div class='form-group'>       
             Nombre d'années <input class='form-control' type='number' name='Annee'/>
          </div>

        <div class='form-group'>
             Email <input class='form-control' type='email' name='Email' >
        </div>

        <div class='form-group'> 
            Ce que j'aime le plus <textarea rows='2' class='form-control'  name='description'></textarea>
        </div>

        <div class='form-group'> 
            Formation A <textarea rows='2' class='form-control' name='formA'></textarea>
        </div>

        <div class='form-group'> 
            Formation B <textarea rows='2' class='form-control' name='formB'></textarea>
        </div>

        <div class='form-group'> 
            Formation C <textarea rows='2' class='form-control' name='formC'></textarea>
        </div>

        
  </div>
        <div class='modal-footer'>
            <button type='submit' name='Gestion_Ajout' class='btn btn-warning btn-lg' style='width: 100%;'> Ajouter </button>
        </div>        
      </form>

  </div>
</div>
</div>
<?php
}
include('footer.php');
?>