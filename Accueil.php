
<?php
include ('header.php');
?>


<!-- Carroussel en brut -->
<div id="carouselExampleIndicators" class="carousel slide carousel-fade">
  <ol class="carousel-indicators">
    <li data-target="#carousel" data-slide-to="0" class="active"></li>
    <li data-target="#carousel" data-slide-to="1"></li>
    <li data-target="#carousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
    	
        <img class="d-block w-75 h-75 mx-auto" src=img/carroussel_4.png alt="First slide"/>
     	
    </div>
    <div class="carousel-item">
    	
        <img class="d-block w-75 h-75 mx-auto" src="img/carroussel_5.jpg" alt="Second slide">
      	
    </div>
    <div class="carousel-item">
    	
        <img class="d-block w-75 h-75 mx-auto" src="img/carroussel_3.jpg" alt="Third slide">
    </div>

  </div>
  
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" class="btn btn-primary"  data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<?php
include ('footer.php');
?>