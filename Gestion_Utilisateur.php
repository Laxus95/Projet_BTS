<?php
include ('header.php');
include ('Connexion_BDD.php');
?>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<title> Gérer utilisateurs </title>

<?php


//récupération des variables de session
if(($_SESSION['profil']=='Administrateur'))
{


  $email2 = $_SESSION['email_session'];
  $id = $_SESSION['id_session'];
  $mdp_user= $_SESSION['mdp_session'];
  $ligne=$_SESSION['ligne_user'];
  }

  $message="";

  if (empty($email2)|| empty($id)) // Intrusion
  {
    $message='Vous ne possédez pas les autorisations nécessaires pour accéder aux informations de ce compte';
    header('Location: Accueil.php');
  }

  else
  {
                  
//Appel de la fonction de connexion à la bdd
    $bdd=BDD();


  ?>

<!-- Tableau présentation des données-->
<div class="container">
	<div class="row">
		<div class="paragraphe">
        
        <div class="col-md-12">
          <h4>Gestion utilisateurs</h4>
          Pour filtrer la recherche, entrez un nom
          <form action="Gestion_Utilisateur.php" method="POST"> 
            <input type="text" placeholder="Entrer un nom" name="filtre"/>
            <button type='submit' name='Filtrage' class='btn btn'> Rechercher </button>
          </form>
          <div class="table-responsive">

                
              <table id="utilisateurs" class="table table-bordred table-striped">
                   
                <thead>
                   
                   
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Email</th>
                    <th>Ligne</th>
                    <th>Profil</th>
                    <th>Modifier</th>                     
                    <th>Supprimer</th>
                </thead>
                <tbody>
    
<?php
    // Requete de récupération des données sur les formateurs suivant utilisation ou non du filtre
          if ($_SESSION['profil']=='Administrateur')
          {
            if (isset($_POST['Filtrage']))
            {
              $nomFiltre=$_POST['filtre'];
              $requete=$bdd->prepare("SELECT id_user,nom, prenom, Profil, email, Ligne FROM utilisateurs WHERE profil !='Administrateur' AND nom like '%".$nomFiltre."%'");
            }

            else
            {
              $requete=$bdd->prepare("SELECT id_user,nom, prenom, Profil, email, Ligne FROM utilisateurs WHERE profil !='Administrateur'");
            }
            
          }
//Execution 
        $requete->execute();
// Boucle des données du tableau pour chaque formateur afin de tout afficher 
        while ($ligne=$requete->fetch())
        {
          $nom=$ligne['nom'];
          $prenom=$ligne['prenom'];
          $email=$ligne['email'];
          $etablissement=$ligne['Ligne'];
          $profil=$ligne['Profil'];
          $id_utilisateur=$ligne['id_user'];
          $DD="DD";
          $id_utilisateur2=$DD.$ligne['id_user'];

// Tableau d'affichage
          print  "<tr>
                    <td width='20%'>".$nom."</td>
                    <td width='20%'>".$prenom."</td>
                    <td width='20%'>".$email."</td>
                    <td width='20%'>".$etablissement."</td>
                    <td width='20%'>".$profil."</td>
                    <td width='20%'><p data-placement='top' data-toggle='tooltip' title='Edit'><button class='btn btn-primary btn-xs' data-title='Edit' data-toggle='modal' data-target='#".$id_utilisateur."'><span class='glyphicon glyphicon-pencil'></span></button></p></td>
                    <td width='20%'><p data-placement='top' data-toggle='tooltip' title='Supprimer'><button class='btn btn-danger btn-xs' data-title='Delete' data-toggle='modal' data-target='#".$id_utilisateur2."'><span class='glyphicon glyphicon-trash'></span></button></p></td>    
                  </tr>";
     



?>

<!-- Modification des données d'un utilisateur -->
<form action="ModifBDD_Admin_Utilisateur.php" method="POST">

<?php 

//Formulaire de modification des données suivant chaque personne du tableau: $id
print"<div class='modal fade' id=".$id_utilisateur." name='id' tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>" ?>
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
          <h4 class="modal-title custom_align" id="Heading">Modifier les données</h4>
        </div>
      <div class="modal-body">
        <div class="form-group">
          
  <?php 
// Champ caché récupérant l'id de l'utilisateur afin d'identifier quel utilisateurs modifier
    print"<input type='hidden' name='id_utilisateur' value=".$id_utilisateur."/>";
// Affectation des valeurs de base des champs du formulaire
    print  " Nom <input class='form-control' type='text' name='Nom' value=".$nom." />
        </div>";

    print  "<div class='form-group'> Prénom      
            <input class='form-control' type='text' name='Prenom' value=".$prenom." />
        </div>
        <div class='form-group'> Email
            <input class='form-control' type='email' name='Email' placeholder='Email' value=".$email." />
        </div>";                      
?>
<!-- Sélection de la ligne -->
           <div> Ligne</div>
              <div>
                <div>
                <input type='radio' name='ligne' value="D&R"/>
                  D&R 
                               
                <input  type='radio' name='ligne' value="C"/>C  

                <input  type='radio' name='ligne' value="EPT4"/>EPT4
                </div>                
                <input  type='radio' name='ligne' value="HBK"/>
                HBK                                         
                <input  type='radio' name='ligne' value="LAJ"/>LAJ
               
                <input  type='radio' name='ligne' value="N&U"/>
                N&U             
                <input  type='radio' name='ligne' value="UTN"/>UTN               
            </div> 
            
<?php

print " <div class='form-group'> Profil      
            <input class='form-control' type='text' name='Profil' value=".$profil." >
        </div> 
  </div>
        <div class='modal-footer'>
            <button type='submit' name='Utilisateur_modif' class='btn btn-warning btn-lg' style='width: 100%;''><span class='glyphicon glyphicon-ok-sign'></span> Modifier </button>
        </div>
  </form>";

    
      ?>

     
            <!-- /.modal-body --> 
            </div> 
            
        </div>
    </div>
 
</div>


 
        </div>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>
    

  <!-- Suppression d'un formateur -->
    <form action="ModifBDD_Admin_Utilisateur.php" method="POST">
    
    <?php print "<div class='modal fade' id=".$id_utilisateur2." tabindex='-1' role='dialog' aria-labelledby='edit' aria-hidden='true'>"; ?>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
            <h4 class="modal-title custom_align" id="Heading">Suppression des données</h4>
          </div>
          <div class="modal-body">
            <?php print "<input type='hidden' name='id_utilisateur' value=".$id_utilisateur.">"; ?>
              <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Etes vous sûr de vouloir supprimer cette personne?</div>
       
      
            <div class="modal-footer ">
              <button type="submit" name='Utilisateur_destruct' class="btn btn-success" > Oui</button>
              <button type="button" class="btn btn-default" data-dismiss="modal"> Non</button>

            </div>
      </form>
         
    

<?php
  }
  ?>
         </tbody>        
      </table>
    <!-- /.modal-content --> 
  </div>
      <!-- /.modal-dialog --> 
    </div>
<?php
}
include('footer.php');
?>