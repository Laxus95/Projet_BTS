

<?php

//Connexion à la Base de donnée
function BDD()
{
	try
	{
		$bdd= new PDO('mysql:host=localhost;dbname=utntrombi; charset=utf8','root','');
		$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	return $bdd;
	}
//En cas d'erreur
	catch (Exception $e)
	{
    	die('Erreur : ' . $e->getMessage());
	}
}
?>

<?php
  function Formateurs($ligne)
  {
  	$bdd=BDD();
 // Requete de récupération des données sur les formateurs
	$statement=$bdd->query("SELECT nom, prenom, description, formation_A, Formation_B, Formation_C, annee, email, ligne FROM formateurs WHERE ligne='$ligne'");
// Boucle des données du tableau pour chaque formateur afin de tout afficher 
	while ($ligne=$statement->fetch())
	{
		$nom=$ligne['nom'];
		$prenom=$ligne['prenom'];
		$description=$ligne['description'];
		$form_A=$ligne['formation_A'];
		$email=$ligne['email'];
		$annee=$ligne['annee'];
		
// Affichage des données	
//Section image	

			echo " <center> <section class='row' style='margin-left: 20%'>";
 
       			echo "<div class='col-xs-2 col-sm-5 col-md-2'>";
         
        			echo "<img class='img-thumbnail' width='80%' height='90%' src=img/".$prenom."_".$nom.".jpg />";
				
				echo "</div>";
//Section informations
				echo"<aside class='col-sm-8 col-xs-5 col-md-5' style='margin-top: 2%'>";
					echo "<info>";
						echo '<table>';
							print "<h4><td class='police'>".$nom."  ".$prenom."</td></h4>";
				
							echo "<tr>";
							
								print "<td class='police2'> Ce qui me plaît le plus dans le métier de formateur : </td>";

							echo "</tr>";
							echo "<tr>";
							if($description=="")
							{
								echo "<td class='police3'>Non renseigné</td>";
							}
								echo "<td class='police3'>".$description."</td>";
							echo "</tr>";
							echo "<tr>";
								echo "<td class='police3'></br> Je suis formateur depuis ".$annee." ans </td>";
							echo "</tr>";
							echo "<tr>";
								echo '<td>'.$email.'</td>';
							echo "</tr>";	
						echo '</table>';

					echo"</info>";

					echo"</aside>";
					print "<div class='col-lg-8 col-lg-offset-2'><hr style='color:#0085CA;background-color:#0085CA; height:2px'></div>";
					
        		

        	echo "</section> </center>";

		
		
	}
  }




   function Evenement()
  {
  	$bdd=BDD();
// Requete de récupération des données sur les formateurs
//On récupère les évènements dont la date n'est pas passé et la publication est autorisée
	$statement=$bdd->query("SELECT Nom, Date_Event,Horaires, Ville, Cp_Event, Rue, Adresse_email, Texte FROM Evenements WHERE Date_Event>CURDATE() AND publication is true");
// Boucle des données du tableau pour chaque formateur afin de tout afficher 
	while ($ligne=$statement->fetch())
	{
		$nom_event=$ligne['Nom'];
		$date_event=$ligne['Date_Event'];
		$horaire_event=$ligne['Horaires'];
		$ville_event=$ligne['Ville'];
		$cp_event=$ligne['Cp_Event'];
		$rue_event=$ligne['Rue'];
		$email_event=$ligne['Adresse_email'];
		$description_event=$ligne['Texte'];
		
// Affichage des données	
//Section image	

			echo " <center> <section class='row' style='margin-left: 20%'>";
 
       			echo "<div class='col-xs-4 col-sm-5 col-md-4'>";
         
        			echo "<img class='img-thumbnail' width='80%' height='90%'style='margin-top:10%' src=img/event_".$date_event.".jpg />";
				
				echo "</div>";
//Section informations
				echo"<aside class='col-sm-12 col-xs-8 col-md-6'>";
					//echo "<info>";

						echo '<table>';
							echo "<tr>";
								print "<h4><a class='police'>".$nom_event." :</a></h4></tr>";
							
							echo "<tr><br>";
								echo "<td class='police2' style='width:50%'> Lieu de l'évènement: <br></td>";
								echo "<td class='police3'>".$rue_event."   ".$ville_event."  ".$cp_event."</td>";
							echo "</tr>";
							
							echo "<tr>";
								echo "<td class='police2'></br> Descriptif de l'évènement:</td>";
							echo "</tr>";
							echo "<tr>";
								echo "<td class='police3' colspan=2>".$description_event."</td>";
							echo "</tr>";
							echo "<tr>";
								print "<td class='police2'></br> Date de l'évènement : </td> <td class='police3'> ";
								print "</br>".$date_event." à  ".$horaire_event."</td>";
							echo"</tr>";

							print "<tr>
								<td class='police4'> Nous contacter: ".$email_event."</td>
								</tr>";		
						echo '</table>';

					

					echo"</aside>";
					print "<div class='col-lg-10 col-lg-offset-10'><hr style='color:#0085CA;background-color:#0085CA; height:2px'></div>";
					
        		

        	echo "</section> </center>";

		
		
	}
  }
  
?>


