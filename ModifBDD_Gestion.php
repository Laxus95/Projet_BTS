<?php
include ('header.php');
include ('Connexion_BDD.php');



$form_A="";
$form_B="";
$form_C="";
$description="";
$nbannee="";

 	

//Affectation des valeurs suivant le formulaire ayant été remplis
if ((isset($_POST['Gestion_modif'])))
{
	$id_form=$_POST['id_formateur'];
	if (empty($_POST['ligne_form']))
	{
			echo "Erreur, veuillez entrer correctement la ligne d'appartenance du formateur";
	}
}
if ((isset($_POST['Gestion_modif']))||(isset($_POST['Gestion_Ajout'])))
	{
		
		
		$email=$_POST['Email'];
		
		
		$nom=$_POST['Nom'];
		$prenom=$_POST['Prenom'];

		if(!empty($_POST['Annee']))
		{
			$nbannee=$_POST['Annee'];
		}
		if(!empty($_POST['description']))
		{
			$description=$_POST['description'];
		}
		if(!empty($_POST['formA']))
		{
			$form_A=$_POST['formA'];
		}
		if(!empty($_POST['formB']))
		{
			$form_B=$_POST['formB'];
		}
		if(!empty($_POST['formC']))
		{
			$form_C=$_POST['formC'];
		}
		
		
		$ligne_form=$_POST['ligne_form'];
		

	}

	
//Vérification que les informations sont bien remplis par une personne connectée
	$email2 = $_SESSION['email_session'];
	$id = $_SESSION['id_session'];
	$mdp_user= $_SESSION['mdp_session'];
	


	$message="";

	if (empty($email2)|| empty($id)) // Intrusion
	{
		$message='Vous ne possédez pas les autorisations nécessaires pour accéder aux informations de ce compte';
	}
	
	else
	{
                  
//Appel de la fonction de connexion à la bdd
		$bdd=BDD();

// Si le formulaire de modification a été saisi :
		if (isset($_POST['Gestion_modif']))
		{ 
			if (($nom=="")|| ($prenom=="")||($email=="")) // Oublis champ
			{
				$message='Veuillez remplir correctement les champs';
			}


			else 
			{
// Préparation de la requete d'insertion des nouvelles données
				if ($_SESSION['profil']=='Administrateur')
				{

				
					$requete=$bdd->prepare('UPDATE formateurs SET email=:email, Nom=:nom, Prenom=:prenom, annee=:nbannee, Description=:description, Formation_A=:form_A, Formation_B=:form_B, Formation_C=:form_C, Ligne=:ligne_form where id_Form=:id');
					$requete->bindParam(':ligne_form', $ligne_form, PDO::PARAM_STR);
					
				}

				if ($_SESSION['profil']=='Gestionnaire')
				{
					$requete=$bdd->prepare('UPDATE formateurs SET email=:email, Nom=:nom, Prenom=:prenom, annee=:nbannee, Description=:description, Formation_A=:form_A, Formation_B=:form_B, Formation_C=:form_C where id_Form=:id');
				}
				$requete->bindParam(':email', $email, PDO::PARAM_STR);
				$requete->bindParam(':id', $id_form, PDO::PARAM_INT);
				$requete->bindParam(':nbannee', $nbannee, PDO::PARAM_INT);
				$requete->bindParam(':description', $description, PDO::PARAM_STR);
				$requete->bindParam(':form_A', $form_A, PDO::PARAM_STR);
				$requete->bindParam(':form_B', $form_B, PDO::PARAM_STR);
				$requete->bindParam(':form_C', $form_C, PDO::PARAM_STR);
				$requete->bindParam(':nom', $nom, PDO::PARAM_STR);
				$requete->bindParam(':prenom', $prenom, PDO::PARAM_STR);
				
//Execution et retour à la page d'accueil
				$requete->execute();

				header('Location: Page_gestion.php');

			}
		}

// Si la suppression a été sélectionnée:


	if (isset($_POST['Gestion_destruct']))
	{
		$id_form=$_POST['id_formateur'];

		$requete=$bdd->prepare('DELETE FROM formateurs WHERE id_Form=:id_form');
		$requete->bindParam(':id_form', $id_form, PDO::PARAM_INT);

		$requete->execute();

		header('Location: Page_gestion.php');
	}
		
// Si l'ajout a été sélectionné
		if (isset($_POST['Gestion_Ajout']))

	{

		if (empty($_POST['ligne_form']))
		{
			echo "Erreur, veuillez entrer correctement la ligne d'appartenance du formateur";
		}
		else
		{
			if (($nom=="")|| ($prenom=="")||($email=="")) // Oublis champ
			{
				$message='Veuillez remplir correctement les champs';
			}
			else
			{
				$ligne=$_POST['ligne_form'];
		
				$requete=$bdd->prepare('INSERT INTO formateurs (Nom,Prenom,Description,Formation_A, Formation_B, Formation_C, annee, email, Ligne) Values (:nom,:prenom, :description, :form_A, :form_B, :form_C, :nbannee, :email, :ligne)');

				$requete->bindParam(':email', $email, PDO::PARAM_STR);
				$requete->bindParam(':ligne', $ligne, PDO::PARAM_STR);
				$requete->bindParam(':nbannee', $nbannee, PDO::PARAM_INT);
				$requete->bindParam(':description', $description, PDO::PARAM_STR);
				$requete->bindParam(':form_A', $form_A, PDO::PARAM_STR);
				$requete->bindParam(':form_B', $form_B, PDO::PARAM_STR);
				$requete->bindParam(':form_C', $form_C, PDO::PARAM_STR);
				$requete->bindParam(':nom', $nom, PDO::PARAM_STR);
				$requete->bindParam(':prenom', $prenom, PDO::PARAM_STR);

				$requete->execute();

				header('Location: Page_gestion.php');
			}



		

		}
	}	
}

echo $message;

	
include ('footer.php');

?>