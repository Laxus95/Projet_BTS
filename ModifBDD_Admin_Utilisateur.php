<?php
include ('header.php');
include ('Connexion_BDD.php');

$email="";
$Profil="";
 	
//Affectation des valeurs suivant le formulaire ayant été remplis
if ((isset($_POST['Utilisateur_modif'])))
{
	$id_utilisateur=$_POST['id_utilisateur'];

	if (empty($_POST['ligne']))
	{
			echo "Erreur, veuillez entrer correctement la ligne d'appartenance de l'utilisateur";
	}
}
if (isset($_POST['Utilisateur_modif']))
	{
		if(!empty($_POST['Email']))
		{
			$email=$_POST['Email'];
		}
		
		$nom=$_POST['Nom'];
		$prenom=$_POST['Prenom'];

		if(!empty($_POST['Profil']))
		{
			$profil=$_POST['Profil'];
		}
		
		

		
		$ligne_utilisateur=$_POST['ligne'];
		

	}

	
//Vérification que les informations sont bien remplis par une personne connectée
	$email2 = $_SESSION['email_session'];
	$id = $_SESSION['id_session'];
	


	$message="";

	if (empty($email2)|| empty($id)) // Intrusion
	{
		$message='Vous ne possédez pas les autorisations nécessaires pour accéder aux informations de ce compte';
	}
	
	else
	{
                  
//Appel de la fonction de connexion à la bdd
		$bdd=BDD();

// Si le formulaire de modification a été saisi :
		if (isset($_POST['Utilisateur_modif']))
		{ 
			if (($nom=="")|| ($prenom=="")) // Oublis champ
			{
				$message='Veuillez remplir correctement les champs';
			}


			else 
			{
// Préparation de la requete d'insertion des nouvelles données
				if ($_SESSION['profil']=='Administrateur')
				{

				
					$requete=$bdd->prepare('UPDATE utilisateurs SET email=:email, Nom=:nom, Prenom=:prenom, Profil=:profil, Ligne=:ligne_utilisateur where id_user=:id_utilisateur');

					$requete->bindParam(':ligne_utilisateur', $ligne_utilisateur, PDO::PARAM_STR);
					$requete->bindParam(':nom', $nom, PDO::PARAM_STR);
					$requete->bindParam(':prenom', $prenom, PDO::PARAM_STR);
					$requete->bindParam(':email', $email, PDO::PARAM_STR);
					$requete->bindParam(':id_utilisateur', $id_utilisateur, PDO::PARAM_INT);
					$requete->bindParam(':profil', $profil, PDO::PARAM_STR);
					
				
				}
				
//Execution et retour à la page d'accueil
				$requete->execute();

				header('Location: Gestion_Utilisateur.php');

			}
		}

// Si la suppression a été sélectionnée:


	if (isset($_POST['Utilisateur_destruct']))
	{
		$id_utilisateur=$_POST['id_utilisateur'];
		
		$requete=$bdd->prepare('DELETE FROM utilisateurs WHERE id_user=:id_utilisateur');
		$requete->bindParam(':id_utilisateur', $id_utilisateur, PDO::PARAM_INT);

		$requete->execute();
		header('Location: Gestion_Utilisateur.php');

		
	}
		
}
echo $message;


	
include ('footer.php');

?>