<?php

include ('header.php');
?>
<title>S'inscrire</title>
<!-- Formulaire inscription (pour l'instant non affiché directement sur le site, il faut entrer le lien directement) -->	
	<div class="form-style-10">
		<h1>Inscription!<span>Enregistrez vous pour avoir accès à toutes les fonctionnalités du site!</span></h1>
		<form action="Inscription.php" method="POST">

    		<div class="section"><span>1</span>Email</div>
    		<div class="inner-wrap">
        		<label>Adresse email <input type="email" placeholder="contact@nom.com" name="mail" /></label>
    		</div>

    		<div class="section"><span>2</span>Mot de passe</div>
        	<div class="inner-wrap">
        		<label>Nom <input type="text" placeholder="Entrer votre nom" name="nom" /></label>
        		<label>Prénom <input type="text" placeholder="Entrez votre Prénom" name="prenom" /></label>
        		<label> Entrez la ligne auquel vous appartenez</label>

        		<table>
        			<tr>
        				<td style='width:40%'><input type="radio" name="ligne" value="D&R"/>
        					D&R
        				</td>

        				<td style='width:20%'><input type="radio" name="ligne" value="C"/>C
        				</td>

        				<td style='width:20%'><input type="radio" name="ligne" value="EPT4"/>EPT4
        				</td>

        				<td style='width:20%'><input type="radio" name="ligne" value="HBK"/>HBK
        				</td>
        			</tr>
        			<tr>	
        				<td style='width:40%'><input type="radio" name="ligne" value="LAJ"/>LAJ
        				</td>

        				<td style='width:33%'><input type="radio" name="ligne" value="N&U"/>
        				N&U
        				</td>

        				<td style='width:20%'><input type="radio" name="ligne" value="UTN"/>UTN
        				</td>
        			</tr>
        		</table>

        		<label>Mot de passe <input type="password" placeholder="Entrez votre mot de passe" name="pass" /></label>
        		<label>Confirmer mot de passe <input type="password" placeholder="Confirmez votre mot de passe" name="confirmepass" /></label>
    		</div>

    		<div class="button-section">
     			<input type="submit" name="Sign Up" />
    		</div>
		</form>
	</div>
<?php

include ('footer.php');
?>