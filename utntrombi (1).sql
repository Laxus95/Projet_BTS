-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 02 mars 2018 à 13:20
-- Version du serveur :  10.1.30-MariaDB
-- Version de PHP :  7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `utntrombi`
--
CREATE DATABASE IF NOT EXISTS `utntrombi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `utntrombi`;

-- --------------------------------------------------------

--
-- Structure de la table `evenements`
--

DROP TABLE IF EXISTS `evenements`;
CREATE TABLE `evenements` (
  `id` int(11) NOT NULL,
  `Nom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Date_Event` date NOT NULL,
  `Horaires` time NOT NULL,
  `Ville` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Cp_Event` int(5) NOT NULL,
  `Rue` varchar(30) CHARACTER SET utf8 NOT NULL,
  `Texte` text CHARACTER SET utf8 NOT NULL,
  `Adresse_email` varchar(40) CHARACTER SET ascii NOT NULL,
  `publication` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `evenements`
--

INSERT INTO `evenements` (`id`, `Nom`, `Date_Event`, `Horaires`, `Ville`, `Cp_Event`, `Rue`, `Texte`, `Adresse_email`, `publication`) VALUES
(1, 'Exemple Evenement', '2018-03-24', '14:00:00', 'St-Denis', 93200, 'Wilson', 'sdfs ds efr sfsz\"é\" ssd ez ghf A ds sdB HBQZH BHJQB JQZHBH // éé\"\'(()àç_è&é\'(n ,zej ntête école \r\nsdfs ds efr sfsz\"é\" ssd ez ghf A ds sdB HBQZH BHJQB JQZHBH // éé\"\'(()àç_è&é\'(n ,zej ntête école \r\nsdfs ds efr sfsz\"é\" ssd ez ghf A ds sdB HBQZH BHJQB JQZHBH // éé\"\'(()àç_è&é\'(n ,zej ntête école \r\nsdfs ds efr sfsz\"é\" ssd ez ghf A ds sdB HBQZH BHJQB JQZHBH // éé\"\'(()àç_è&é\'(n ,zej ntête école ', 'test4@sncf.fr', 1),
(2, 'Passé', '2018-03-15', '14:00:00', 'St-Denis', 93200, 'Wilson', 'Cet évènement est déjà passé.', 'wilson@sncf.fr', 1),
(3, 'Evènement 2', '2018-03-28', '11:58:00', 'St-Denis', 93200, 'Wilson', 'Saepissime igitur mihi de amicitia cogitanti maxime illud considerandum videri solet, utrum propter imbecillitatem atque inopiam desiderata sit amicitia, ut dandis recipiendisque meritis quod quisque minus per se ipse posset, id acciperet ab alio vicissimque redderet, an esset hoc quidem proprium amicitiae, sed antiquior et pulchrior et magis a natura ipsa profecta alia causa. Amor enim, ex quo amicitia nominata est, princeps est ad benevolentiam coniungendam. Nam utilitates quidem etiam ab iis percipiuntur saepe qui simulatione amicitiae coluntur et observantur temporis causa, in amicitia autem nihil fictum est, nihil simulatum et, quidquid est, id est verum et voluntarium.\r\n\r\nQuam ob rem ut ii qui superiores sunt submittere se debent in amicitia, sic quodam modo inferiores extollere. Sunt enim quidam qui molestas amicitias faciunt, cum ipsi se contemni putant; quod non fere contingit nisi iis qui etiam contemnendos se arbitrantur; qui hac opinione non modo verbis sed etiam opere levandi sunt.\r\n\r\nAuxerunt haec vulgi sordidioris audaciam, quod cum ingravesceret penuria commeatuum, famis et furoris inpulsu Eubuli cuiusdam inter suos clari domum ambitiosam ignibus subditis inflammavit rectoremque ut sibi iudicio imperiali addictum calcibus incessens et pugnis conculcans seminecem laniatu miserando discerpsit. post cuius lacrimosum interitum in unius exitio quisque imaginem periculi sui considerans documento recenti similia formidabat.', 'wilson@sncf.fr', 1);

-- --------------------------------------------------------

--
-- Structure de la table `formateurs`
--

DROP TABLE IF EXISTS `formateurs`;
CREATE TABLE `formateurs` (
  `id_Form` int(11) NOT NULL,
  `Nom` varchar(40) CHARACTER SET utf8 NOT NULL,
  `Prenom` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Formation_A` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Formation_B` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Formation_C` varchar(255) CHARACTER SET utf8 NOT NULL,
  `annee` int(11) NOT NULL,
  `email` varchar(40) CHARACTER SET ascii NOT NULL,
  `Ligne` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `formateurs`
--

INSERT INTO `formateurs` (`id_Form`, `Nom`, `Prenom`, `Description`, `Formation_A`, `Formation_B`, `Formation_C`, `annee`, `email`, `Ligne`) VALUES
(1, 'Rodrigues', 'Ricardo', 'Le transfert de connaissances', 'Conducteur filière matériel', 'Chef de la manoeuvre', 'PRAP', 7, 'rodrigues@sncf.fr', 'D&R'),
(2, 'Caudrillier', 'Philippe', 'Humain', 'TB initial', 'Formation transilien pour les agents mutés', 'Formation EM', 11, '', 'D&R'),
(4, 'Blondet', 'Delphine', 'Transmettre et partager', 'Vente et Après Vente Transilien', 'Accessibilité PMR', 'Suivi ASCVG', 4, '', 'C'),
(5, 'Bertaz', 'Nathalie', 'Le contact humain et le partage de connaissances', 'EGT LC PRG VO00036', 'Personnel d\'assistance', 'Risques Professionnels', 1, '', 'C'),
(6, 'Hauet', 'Nathalie', '', 'Nouveaux embauchés ', 'Accessibilité ', '', 6, '', 'LAJ'),
(7, 'Kivel', 'Laurent', 'L\'échange', 'Juridique, Placement et IncivilitéAccessibilté', 'Accessibilité', 'Produits et Services TN', 3, '', 'LAJ'),
(8, 'Rigal', 'Delphine', 'Le fait de transmettre et apprendre ;  le travaile en réseau avec les autres formateurs', 'SAV Navigo', 'Juridique placement', 'Accessibilité', 4, '', 'LAJ'),
(9, 'Puleggi', 'Nadia', 'La remise en question', 'Accompagner l\'accessibilité', 'PMR', 'MANAGER TRACTION ZD MTA', 3, '', 'UTN'),
(10, 'Manciot', 'Michel', '', '', '', '', 9, '', 'UTN'),
(11, 'Denoyelle', 'Jean-François', 'Transmettre,faire aimer notre métier', 'Juridique placement', 'PMR', '', 5, '', 'EPT4'),
(12, 'Patin', 'Frédéric', '', 'Sécurité', 'Révision commerciale', 'Préparation CAF', 10, '', 'EPT4');

-- --------------------------------------------------------

--
-- Structure de la table `ligne_t`
--

DROP TABLE IF EXISTS `ligne_t`;
CREATE TABLE `ligne_t` (
  `nom_ligne` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ligne_t`
--

INSERT INTO `ligne_t` (`nom_ligne`) VALUES
('C'),
('D&R'),
('EPT4'),
('HBK'),
('LAJ'),
('N&U'),
('UTN');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE `utilisateurs` (
  `id_user` int(11) NOT NULL,
  `email` varchar(40) CHARACTER SET ascii NOT NULL,
  `mdp` varchar(40) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `nom` varchar(40) CHARACTER SET utf8 NOT NULL,
  `prenom` varchar(40) CHARACTER SET utf8 NOT NULL,
  `Ligne` varchar(10) NOT NULL,
  `Profil` varchar(20) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id_user`, `email`, `mdp`, `nom`, `prenom`, `Ligne`, `Profil`) VALUES
(4, 'test2@sncf.fr', 'password', 'DUPONT', 'Jean', 'C', 'utilisateur'),
(6, 'test5@sncf.fr', 'pass', 'Maxime', 'Dubois', 'N&U', ''),
(8, 'test6@sncf.fr', '1234', '', '', '', ''),
(9, 'test4@sncf.fr', 'portugal', '', '', '', ''),
(10, 'test7@sncf.fr', 'pass', '', '', '', ''),
(11, 'test8@sncf.fr', 'portugal', '', '', '', ''),
(12, 'francisco@sncf.fr', 'portugal', 'Da Silva', 'Manon', 'D&R', 'Administrateur'),
(13, 'manon@sncf.fr', 'portugal', 'Silva', 'Manon', 'C', 'Gestionnaire'),
(14, 'dylane@sncf.fr', 'password', 'Da Silva', 'Dylane', 'D&R', 'Gestionnaire');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `evenements`
--
ALTER TABLE `evenements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `formateurs`
--
ALTER TABLE `formateurs`
  ADD PRIMARY KEY (`id_Form`),
  ADD KEY `Ligne` (`Ligne`);

--
-- Index pour la table `ligne_t`
--
ALTER TABLE `ligne_t`
  ADD PRIMARY KEY (`nom_ligne`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `unique` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `evenements`
--
ALTER TABLE `evenements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `formateurs`
--
ALTER TABLE `formateurs`
  MODIFY `id_Form` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `formateurs`
--
ALTER TABLE `formateurs`
  ADD CONSTRAINT `formateurs_ibfk_1` FOREIGN KEY (`Ligne`) REFERENCES `ligne_t` (`nom_ligne`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
